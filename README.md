README file of python test assesment

Content of the project:
Objective of this project - to create a simple REST API of social network.

Technologies used:
Django (version - 3.1.7)
djangorestframework (version - 3.12.4)
djangorestframework-simplejwt (version - 4.6.0)


Setup:
1. Clone the repo: git clone https://gitlab.com/afzalyusupov/rest_api_for_social_network
2. Open the directory, which you cloned from gitlab in your preferred IDE.
3. Then install the dependencies with the following command:
		$ pip install -r requirements.txt
		
4. Once pip has finished downloading the dependencies, run the following commands:
		$ python manage.py migrate
		$ python manage.py createsuperuser
		$ python manage.py runserver

5. And navigate to http://127.0.0.1:8000/api/swagger-ui/

from django.contrib.auth.models import User
from django.db import models
from django.utils.text import slugify


class Post(models.Model):
    title = models.CharField(max_length=120)
    slug = models.CharField(max_length=255)
    body = models.TextField()
    likes = models.ManyToManyField(User, related_name='post_like')
    owner = models.ForeignKey('auth.User', related_name='posts', on_delete=models.CASCADE)
    updated_at = models.DateField(auto_now=True, verbose_name='Update at')
    created_at = models.DateField(auto_now_add=True, verbose_name='Created at')

    def __str__(self):
        return self.title

    def number_of_likes(self):
        return self.likes.count()

    def _generate_unique_slug(self):
        unique_slug = slugify(self.title, allow_unicode=True)
        num = 1
        while Post.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(unique_slug, num)
            num += 1
        return unique_slug

    class Meta:
        ordering = ['created_at']


class ActivityLog(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    last_login = models.DateTimeField(auto_now=True, verbose_name='login last time')
    request_url = models.CharField(max_length=200, verbose_name="last request of user's")
    request_method = models.CharField(max_length=50, verbose_name="method of last request")

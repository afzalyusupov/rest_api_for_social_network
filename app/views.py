from drf_spectacular.utils import extend_schema
from rest_framework import generics, status
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from app.models import Post
from app.serializer import RegisterSerializer, UserSerializer, PostSerializer


class RegisterApi(generics.GenericAPIView):
    serializer_class = RegisterSerializer

    @extend_schema(
        request=RegisterSerializer,
        responses={201: RegisterSerializer},
    )
    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "user": UserSerializer(user, context=self.get_serializer_context()).data,
            "message": "User Created Successfully",
        }, status=status.HTTP_201_CREATED)


class PostLikeAPIView(generics.GenericAPIView):
    queryset = Post.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    @extend_schema(
        summary="add and remove like from post",
        description='id - post id',
    )
    def get(self, request, *args, **kwargs):
        post_id = kwargs.get('pk')

        if post := Post.objects.filter(pk=post_id).first():
            return Response({
                "message": "Post not found!",
                "success": False
            }, status=status.HTTP_404_NOT_FOUND)

        if post.likes.filter(id=request.user.id).exists():
            post.likes.remove(request.user)
            message = "like is removed"
        else:
            post.likes.add(request.user)
            message = "like is added"

        return Response({
            "message": message,
            "success": True
        })


class PostListCreateAPIView(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self, *args, **kwargs):
        return self.queryset.filter(owner=self.request.user)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)

    @extend_schema(
        summary="adding post",
        description='something',
    )
    def post(self, request, *args, **kwargs):
        return super().post(request, *args, **kwargs)


class AnaliticsListAPIView(generics.ListAPIView):
    serializer_class = PostSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        try:
            """
            Optionally restricts the returned purchases to a given user,
            by filtering against a username query parameter in the URL.
            """
            queryset = Post.objects.all()
            date_from = self.request.query_params.get('date_from')
            date_to = self.request.query_params.get('date_to')
            if date_from:
                queryset = queryset.filter(created_at__gte=date_from)
            if date_to:
                queryset = queryset.filter(created_at__lte=date_to)
            return queryset
        except:
            return None

    @extend_schema(
        summary="add and remove like from post",
        description='id - post id',
    )
    def list(self, request, *args, **kwargs):
        try:
            data = PostSerializer(self.get_queryset(), many=True).data
            context = {
                "data": data,
                "message": "Contents returned successfully",
                "success": True
            }
            return Response(context, status=status.HTTP_200_OK)

        except Exception as error:
            return Response({
                'error': str(error), 'success': False, 'message': 'Failed to get contents.'
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)

    @extend_schema(
        summary="get analitics",
        description='something',
    )
    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)

from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.test import APITestCase


class MemberTestCase(APITestCase):

    def test_register_(self):
        self.url = "/api/register/"
        data = {
            "username": "john_doe",
            "password": "Gfj&f3",
            "first_name": "John",
            "last_name": "Doe",
            "email": "john_doe345@gmail.com"
        }
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_token_login_fail_incorrect_credentials(self):
        self.url = "/api/login/"
        data = {
            'username': 'Alex',
            'password': 'FGgh32f'
        }
        resp = self.client.post(self.url, data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_token_login_success(self):
        user = User.objects.create_user(
            username='admin',
            password='admin',
            email='admin@mail.ru'
        )
        self.url = "/api/login/"
        data = {
            'username': 'admin',
            'password': 'admin'
        }




        resp = self.client.post(self.url, data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
